﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace AutoPrint_Client.Modules
{
    /// <summary>
    /// Статусы печати Все
    /// </summary>
    public enum CodePrintStatus
    {
        /// <summary>
        /// Общая ошибка печати
        /// </summary>
        ErrorPrinting   = 0,
        /// <summary>
        /// Внутрення ошибка печати
        /// </summary>
        ErrorInner      = 10,
        /// <summary>
        /// Ошибка по времени
        /// </summary>
        ErrorTimeOut    = 11,
        /// <summary>
        /// Готов к печати
        /// </summary>
        ReadyOnPrint    = 1,
        /// <summary>
        /// Принтер не в сети
        /// </summary>
        PrinterOffline  = 2,
        /// <summary>
        /// Ожидаем печати
        /// </summary>
        WaitToPrint     = 3,
        /// <summary>
        /// Печать
        /// </summary>
        Printing        = 5,
        /// <summary>
        /// Печать окончена
        /// </summary>
        EndPrint        = 7,
        /// <summary>
        /// Неизвестная ошибка
        /// </summary>
        UNKNOW          = 9
        
    }

    /// <summary>
    /// Расшифровка информации об статусе печати
    /// </summary>
    public class StatusInfo
    {

        public string ErrorDesc = "";

        public CodePrintStatus CurrentStatus;
        public string FileName;
        public DateTime LastInfoData;
        public bool FirstProtocol = true;

        public StatusInfo(string FileName)
        {
            ParseFile(FileName);
        }

        private void ParseFile(string FileName)
        {
            try
            {
                FileStream FS = new FileStream(FileName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                StreamReader SR = new StreamReader(FS);
                this.CurrentStatus = intToCodeStatus(Convert.ToInt32(SR.ReadLine()));

                this.FileName = SR.ReadLine();
                string DateStr = SR.ReadLine();
                if (DateStr != "")
                {
                    this.LastInfoData = Convert.ToDateTime(DateStr);
                    FirstProtocol = false;
                    TimeSpan Span = DateTime.Now - LastInfoData;
                    if (Span.TotalMinutes > 5) this.CurrentStatus = CodePrintStatus.ErrorTimeOut;
                }

            }
            catch (Exception ex)
            {
                ErrorDesc = ex.Message;
                CurrentStatus = CodePrintStatus.ErrorInner;
            }
        }

        /// <summary>
        /// Вернуть статус по цифровому коду
        /// </summary>
        /// <param name="status"></param>
        /// <returns></returns>
        private CodePrintStatus intToCodeStatus(int status)
        {
            switch (status)
            {
                case 0: return CodePrintStatus.ErrorPrinting;
                case 1: return CodePrintStatus.ReadyOnPrint;
                case 2: return CodePrintStatus.PrinterOffline;
                case 3: return CodePrintStatus.WaitToPrint;
                case 5: return CodePrintStatus.Printing;
                case 7: return CodePrintStatus.EndPrint;
                    
                default: return CodePrintStatus.UNKNOW;
            }
        }

           
    }
}
