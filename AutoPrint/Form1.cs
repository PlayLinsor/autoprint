﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.IO;
using System.Printing;
using System.Threading;
using System.Reflection;
using System.Windows.Forms;

namespace AutoPrint
{
    public partial class Form1 : Form
    {
        // Обновление статуса
        const int TimeRefresh = 250;
        int CurrentTimeRefresh = 0;

        // 
        PrintSystemJobInfo xpsPrintJob;
        string FilePrint = "";
        string FileLogPrint = "";
        
        /// <summary>
        /// Числовые статусы
        /// </summary>
        enum StatusPrint {Ready=1,Offline=2, Wait=3, Printing=5 ,Sussec=7,Error=0}

        public Form1()
        {
            InitializeComponent();
            Init(); 
            
        }

        private void Init()
        {
            listBox1.Items.Clear();
            TimerRefreshStatus.Enabled = true;
            Log("Версия протокола обмена 1.1");

            if (Properties.Settings.Default.AutoUpdate)
            {
                Properties.Settings.Default.Upgrade();
                Properties.Settings.Default.AutoUpdate = false;
            }
            if (textBox1.Text != Properties.Settings.Default.Path)
            {
                textBox1.Text = Properties.Settings.Default.Path;
                fileSystemWatcher1.Path = textBox1.Text;
            }

            if (Directory.Exists(textBox1.Text))
            {
                fileSystemWatcher1.Path = textBox1.Text;
                Log("Готов к работе :)");
                FileLogPrint = textBox1.Text + "\\status.txt";
                LogInFile(StatusPrint.Ready);
            }
            else
            {
                Log("ВНИМАНИЕ ОТСУТСТВУЕТ ДИРЕКТОРИЯ ОТСЛЕЖИВАНИЯ");
                LogInFile(StatusPrint.Error);
            }
            
            Text = "Auto XPS Print v." + Application.ProductVersion + " Наумко Д.В.";
            notifyIcon1.BalloonTipTitle = Text;
        }

        private void fileSystemWatcher1_Created(object sender, System.IO.FileSystemEventArgs e)
        {
            if (FilePrint != e.FullPath)
            {
                Log("На печать:" + e.Name);
                FilePrint = e.FullPath;
                LogInFile(StatusPrint.Wait);
                Thread.Sleep(1000);
                bool isNotFinish = true;
                while (isNotFinish)
                {
                    if (!CheckIfFileIsBeingUsed(e.FullPath))
                    {
                        isNotFinish = false;
                        try
                        {
                            PrintIt(e.FullPath);
                            LogInFile(StatusPrint.Printing);
                            Log("Напечатано:" + e.Name);
                            DeleteTimer.Enabled = true;
                        }
                        catch (Exception ex)
                        {
                            Log("!!!-" + ex.InnerException.Message);
                            LogInFile(StatusPrint.Error);
                            FileStream FS = File.Open( textBox1.Text + string.Format("//ErrorLog_{0}.txt",DateTime.Now.ToString().Replace(':','-')), FileMode.OpenOrCreate);
                            StreamWriter SW = new StreamWriter(FS);
                            SW.Write(ex.ToString());
                            SW.Close();
                            FS.Close();
                        }
                    }
                    else
                    {
                        Log("Ожидание освобождения файла");
                        Thread.Sleep(1000);
                    }
                }
            }
            else
            {
                Log(string.Format("Дубль {0}", e.Name));
            }
        }

        #region Дополнительные Функции

        // Проверка файла на использование кем то другим.
        public bool CheckIfFileIsBeingUsed(string fileName)
        {
            try
            {
                FileStream FS = File.Open(fileName, FileMode.Open, FileAccess.Read, FileShare.Read);
                FS.Close();
            }
            catch
            {
                return true;
            }
            return false;
        }

        // Печатаем XPS Файл
        private void PrintIt(string FileName)
        {
            LocalPrintServer localPrintServer = new LocalPrintServer();
            PrintQueue defaultPrintQueue = LocalPrintServer.GetDefaultPrintQueue();
            if (!defaultPrintQueue.IsOffline)
            {
                xpsPrintJob = defaultPrintQueue.AddJob(FileName, FileName, false);
                DeleteTimer.Enabled = true;
            }
            else LogInFile(StatusPrint.Offline);
        }

        // TrayIcon Сообщение
        public void MessageTrayIcom(string MessageText)
        {
            notifyIcon1.BalloonTipTitle = notifyIcon1.Text;
            notifyIcon1.BalloonTipIcon = ToolTipIcon.Info;
            notifyIcon1.BalloonTipText = MessageText;
            notifyIcon1.ShowBalloonTip(5000);
        }

        // Запись в лог файл
        private void Log(string Text)
        {
            listBox1.Items.Add(Text);
        }

        private void LogInFile(StatusPrint sp)
        {
            try
            {
                FileStream FS = File.Open(FileLogPrint, FileMode.Create, FileAccess.ReadWrite, FileShare.ReadWrite);
                StreamWriter SW = new StreamWriter(FS);
                SW.WriteLine((byte)sp);
                SW.WriteLine(FilePrint);
                SW.WriteLine(DateTime.Now);
                SW.Close();
                FS.Close();
                CurrentTimeRefresh = 0;
            }
            catch (Exception ex)
            {
                Log(ex.Message);
            }
        }
        
        #endregion

        #region Интерфейсные надстройки 
        private void Form1_Shown(object sender, EventArgs e) 
        { Visible = false; }

        private void открытьToolStripMenuItem_Click(object sender, EventArgs e)
        { Visible = true; }

        private void button2_Click(object sender, EventArgs e)
        { Visible = false; }

        private void выходToolStripMenuItem_Click(object sender, EventArgs e)
        { Application.Exit(); }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox1.Text != fileSystemWatcher1.Path)
            {
                try
                {
                    fileSystemWatcher1.Path = textBox1.Text;
                    Properties.Settings.Default.Path = textBox1.Text;
                    Properties.Settings.Default.Save();
                    MessageTrayIcom("Настройки сохранены");
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }

            Visible = false;

        }

        private void notifyIcon1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            MessageTrayIcom("Автоматическая печать XPS документов\nАвтор: Наумко Дмитрий playlinsor@gmail.com");
        }

        #endregion

        private void DeleteTimer_Tick(object sender, EventArgs e)
        {
            if (!CheckIfFileIsBeingUsed(FilePrint))
            {
                File.Delete(FilePrint);
                Log("Исходник удален");
                LogInFile(StatusPrint.Sussec);
                DeleteTimer.Enabled = false;
                StatusTimer.Enabled = true;

            }
        }

        private void StatusTimer_Tick(object sender, EventArgs e)
        {
            LogInFile(StatusPrint.Ready);
            StatusTimer.Enabled = false;
        }

        private void fileSystemWatcher1_Changed(object sender, FileSystemEventArgs e)
        {

        }

        private void TimerRefreshStatus_Tick(object sender, EventArgs e)
        {
            CurrentTimeRefresh++;
            if (CurrentTimeRefresh==TimeRefresh) LogInFile(StatusPrint.Ready);
        }


    }
}
